package com.mangroo.application.web;

import com.mangroo.application.ApplicationConfiguration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@RunWith(MockitoJUnitRunner.class)
public class ApplicationControllerTest {

    private MockMvc mockMvc;

    @InjectMocks
    ApplicationController applicationController;

    @Mock
    private ApplicationConfiguration applicationConfiguration;

    @Before
    public void before() {
        mockMvc = MockMvcBuilders.standaloneSetup(applicationController)
                .build();
    }

    @Test
    public void getApplicationVersionNumberOK() throws Exception {
        given(applicationConfiguration.getName()).willReturn("Mock ApplicationName");

        MockHttpServletResponse response = mockMvc.perform(
                get("/application/version")).andReturn().getResponse();
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
    }
}
